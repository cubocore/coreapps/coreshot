project(
    'CoreShot',
    'c',
	'cpp',
	version: '5.0.0',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-DVERSION_TEXT="@0@"'.format( meson.project_version() ), language : 'cpp' )

add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

GlobalInc = include_directories( [ '.' ] )

Qt = import( 'qt6' )
QtDeps = dependency(
	'qt6',
	modules: [ 'Core', 'Gui', 'Widgets' ]
)

CPrimeCore    = dependency( 'cprime-core-qt6' )
CPrimeGui     = dependency( 'cprime-gui-qt6' )
CPrimeWidgets = dependency( 'cprime-widgets-qt6' )

X11           = dependency( 'x11' )

Headers = [
    'fixx11h.h',
    'settings.h',
]

MocHeaders = [
    'coreshot.h',
    'coreshotdialog.h',
    'previewwidget.h',
]

Sources = [
    'coreshot.cpp',
    'coreshotdialog.cpp',
    'main.cpp',
    'previewwidget.cpp',
    'settings.cpp',
]

Mocs = Qt.compile_moc(
    headers : MocHeaders,
    dependencies: QtDeps
)

UIs = Qt.compile_ui(
    sources: [
        'coreshot.ui',
        'coreshotdialog.ui'
    ]
)

# Resources = Qt.compile_resources(
# 	name: '',
# 	sources: ''
# )

coreshot = executable(
    'coreshot', [ Sources, Mocs, UIs ],
    dependencies: [QtDeps, CPrimeCore, CPrimeGui, CPrimeWidgets, X11],
    include_directories: GlobalInc,
    install: true
)

install_data(
    'cc.cubocore.CoreShot.desktop',
    install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'applications' )
)

install_data(
    'cc.cubocore.CoreShot.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)
