/*
    *
    * This file is a part of CoreShot.
    * A screen capture utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QResizeEvent>
#include <QFileInfo>

#include "previewwidget.h"


PreviewWidget::PreviewWidget(QWidget *parent) : QLabel(parent)
{
    setAlignment(Qt::AlignCenter);
    setFocus();
}

PreviewWidget::~PreviewWidget()
{
}

int PreviewWidget::heightForWidth(int width) const
{
    return cOriginalPixmap.isNull() ? this->height() : ((qreal)cOriginalPixmap.height()*width)/cOriginalPixmap.width();
}

QSize PreviewWidget::sizeHint() const
{
    int w = this->width();
    return QSize( w, heightForWidth(w) );
}

QPixmap PreviewWidget::scaledPixmap() const
{
    float ratio = devicePixelRatioF();
    auto scaled = cOriginalPixmap.scaled(size() * ratio, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    scaled.setDevicePixelRatio(ratio);

    return scaled;
}

void PreviewWidget::resizeEvent(QResizeEvent *event)
{
	event->accept();

    if( !cOriginalPixmap.isNull() )
		setPixmap( scaledPixmap() );
}

void PreviewWidget::setOriginalPixmap(const QPixmap &pix)
{
    cOriginalPixmap = pix;
    QLabel::setPixmap(scaledPixmap());
}

const QPixmap &PreviewWidget::originalPixmap() const
{
    return cOriginalPixmap;
}
