/*
    *
    * This file is a part of CoreShot.
    * A screen capture utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef CORESHOT_H
#define CORESHOT_H

#include <QWidget>
#include <QPixmap>
#include <QCloseEvent>

#include "settings.h"


namespace Ui {
    class coreshot;
}

class coreshot : public QWidget {
    Q_OBJECT

public:
    explicit coreshot(QWidget *parent = nullptr);
    ~coreshot();

    void setPixmap(QPixmap &pix);

protected:
    void closeEvent(QCloseEvent *cEvent) override;

private slots:
    void on_save_clicked();
    void on_saveAs_clicked();
    void on_cancel_clicked();
    void on_newShot_clicked();
    void on_openInEditor_clicked();
    void on_shareIt_clicked();

private:
    Ui::coreshot    *ui;
    settings        *smi;
    int             uiMode;
    QSize           listViewIconSize, toolsIconSize, windowSize;
    QString         saveLocation, files;
    bool            saveClipboard = false, windowMaximized;

    void loadSettings();
    void startSetup();
    void setupIcons();
};

#endif // CORESHOT_H
