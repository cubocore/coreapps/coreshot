/*
    *
    * This file is a part of CoreShot.
    * A screen capture utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef CORESHOTDIALOG_H
#define CORESHOTDIALOG_H

#include <QtWidgets>
#include <QPixmap>

#include "settings.h"


class QMouseEvent;

namespace Ui {
    class coreshotdialog;
}

class coreshotdialog : public QWidget {

    Q_OBJECT

public:

	enum CaptureType {
		FullScreen,
		ActiveWindow,
		SelectArea
	};

    explicit coreshotdialog(QString fileName = QString(), int delay = 0, QWidget *parent = nullptr);
    ~coreshotdialog();

	void capture(CaptureType type);

private:
    Ui::coreshotdialog *ui;
    settings *smi;
    QPixmap m_pixmap;
    QRect snapArea, lastgeom;
    bool mousegrabbed, useSystemNotification;
    QPoint pt_click;
    QWidget *areaOverlay;
	QString mFileName;
	int mDelay = 0;
    int afterShotTaken = 0;
    QString saveLocation;
    QSize toolsIconSize;

	void shootActiveWindow();
	void shootFullScreen();
	void shootSelectArea();

    QRect pointsToRect(QPoint pt1, QPoint pt2);
    WId getActiveWindowId();
    QRect getWindowFrame(WId wid);
    void passTo();
    void startsetup();
    void loadSettings();
    void setupIcons();

    Display *mDisplay = nullptr;
    WId root;

private slots:
    void getPix();
    void on_delayCapture_clicked();
    void on_afterShotTaken_clicked();

protected:
    void mousePressEvent(QMouseEvent *ev) override;
    void mouseMoveEvent(QMouseEvent *ev) override;
    void mouseReleaseEvent(QMouseEvent *ev) override;
    void closeEvent(QCloseEvent *event) override;
};

#endif // CORESHOTDIALOG_H
