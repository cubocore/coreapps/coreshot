/*
    *
    * This file is a part of CoreShot.
    * A screen capture utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QMessageBox>
#include <QPushButton>
#include <QTimer>
#include <QScreen>
#include <QDebug>
#include <QDir>
#include <QDateTime>
#include <QMouseEvent>
#include <QFileInfo>
#include <QWindow>

#include <qpa/qplatformnativeinterface.h>

#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>
#include <cprime/messageengine.h>

#include "previewwidget.h"
#include "coreshot.h"
#include "coreshotdialog.h"
#include "ui_coreshotdialog.h"

#include <QGuiApplication>
#include <QScreen>
#include <QPixmap>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <xcb/xproto.h>
#include "fixx11h.h"

coreshotdialog::coreshotdialog(QString file, int delay, QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::coreshotdialog)
    , smi(new settings)
	, mFileName(file)
    , mDelay(delay)
{
	ui->setupUi(this);

	mousegrabbed = false;
	areaOverlay = nullptr;
    loadSettings();
    setupIcons();
	startsetup();

	// Set window as a framless
    setAttribute(Qt::WA_ShowWithoutActivating);
    setWindowFlags(Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowDoesNotAcceptFocus);
    setAttribute(Qt::WA_X11NetWmWindowTypeDock, true);
    setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));

	// Set the window size & position

    QScreen *scr = QGuiApplication::primaryScreen();
    resize(scr->availableGeometry().width() * .4, scr->availableGeometry().height() * .06);
    setMinimumSize(scr->availableGeometry().width() * .4, scr->availableGeometry().height() * .06);
    setGeometry(0, scr->availableGeometry().height() * .06, scr->availableGeometry().width() * .4, scr->availableGeometry().height() * .06);

	int mx = (scr->availableGeometry().width() / 2) - ((this->width() / 2) + 10);
	int my = (scr->availableGeometry().height()) - (this->height() + 15);
	this->move(mx, my);

    // all toolbuttons icon size in toolBar
    QList<QToolButton *> toolBtns2 = this->findChildren<QToolButton *>();
    for (QToolButton *b: toolBtns2) {
        if (b) {
            b->setIconSize(toolsIconSize);}
    }

	auto *x11Application = qGuiApp->nativeInterface<QNativeInterface::QX11Application>();
	if ( x11Application ) {
		mDisplay = x11Application->display();
	}
}

coreshotdialog::~coreshotdialog()
{
	delete smi;
	delete ui;
}

/**
 * @brief Loads application settings
 */
void coreshotdialog::loadSettings()
{
    useSystemNotification = smi->getValue("CoreApps", "UseSystemNotification");
    afterShotTaken = smi->getValue("CoreShot", "AfterShotTaken");
    mDelay = smi->getValue("CoreShot", "Delay");
    saveLocation = QString(smi->getValue("CoreShot", "SaveLocation"));
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
}

void coreshotdialog::setupIcons(){
    ui->captureScreen->setIcon(CPrime::ThemeFunc::themeIcon( "preferences-desktop-display-symbolic", "preferences-desktop-theme", "desktop" ));
    ui->captureScreen->setToolTip( "Capture whole screen" );

    ui->captureWindow->setIcon(CPrime::ThemeFunc::themeIcon( "zoom-original-symbolic", "zoom-original", "zoom-original" ));
    ui->captureWindow->setToolTip( "Capture Window" );

    ui->captureSelection->setIcon(CPrime::ThemeFunc::themeIcon( "edit-select-all-symbolic", "edit-select-all", "edit-select-all" ));
    ui->captureSelection->setToolTip( "Capture Selection" );
}

void coreshotdialog::capture(coreshotdialog::CaptureType type)
{
	if (type == FullScreen) {
		hide();
        QTimer::singleShot((ui->delayCapture->text().toInt() * 1000) + 200, this, &coreshotdialog::shootFullScreen);
	} else if (type == ActiveWindow) {
		hide();
        QTimer::singleShot((ui->delayCapture->text().toInt() * 1000) + 200, this, &coreshotdialog::shootActiveWindow);
	} else if (type == SelectArea) {
		// Dialog needs to be visible for getting select area
		if (!isVisible()) {
			setWindowOpacity(0);
			show();
		}
		QTimer::singleShot(50, this, &coreshotdialog::shootSelectArea);
	}
}

void coreshotdialog::startsetup()
{
    ui->delayCapture->setText("Delay(s): " + QString::number(mDelay));

    // set text in the afterShotTaken button
    if ( afterShotTaken == 0 ){
        ui->afterShotTaken->setText("Open Image Editor");
    } else if ( afterShotTaken == 1 ){
        ui->afterShotTaken->setText("Open Preview");
    } else if ( afterShotTaken == 2 ){
        ui->afterShotTaken->setText("Auto Save");
    }

	connect(ui->captureScreen, &QToolButton::clicked, [&]() {
		capture(FullScreen);
	});

	connect(ui->captureWindow, &QToolButton::clicked, [&]() {
		capture(ActiveWindow);
	});

	connect(ui->captureSelection, &QToolButton::clicked, [&]() {
		capture(SelectArea);
	});

	connect(ui->cancel, &QToolButton::clicked, this, &coreshotdialog::close);

    ui->cancel->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));

}

void coreshotdialog::passTo()
{
	/*
		*afterShootTaken will be considered when no command line options are given.
		* If any cli options are given, autosave the taken screenshot
	*/
	QStringList argv(qApp->arguments());

	if (argv.count() > 1) {
		// If we're capturing active window, autosave
		if (argv.contains("-w") or argv.contains("--active-window")) {
            afterShotTaken = 2;
		}

		// If we're capturing fullscreen, autosave
		else if (argv.contains("-f") or argv.contains("--fullscreen")) {
            afterShotTaken = 2;
		}

		// If we're capturing selection, autosave
		else if (argv.contains("-s") or argv.contains("--selection")) {
            afterShotTaken = 2;
		}

		// If the user has specified the delay, ignore it
		else if (argv.contains("-d") or argv.contains("--delay")) {}

		// The user has specified the filename
		else {
            afterShotTaken = 2;
		}
	}

    if (afterShotTaken == 2) {  // saves auto
		QString dirName = saveLocation;

		if (!CPrime::FileUtils::isDir(dirName)) {
			qDebug() << "func(passTo) : Error : Invalid saving directory :" << dirName;
			qDebug() << "creating " << saveLocation;
			QDir().mkpath(saveLocation);
		}

		// Remove the last '/' if exists
		dirName = QDir(dirName).path();

        if (dirName.length()) {
			QString file = mFileName;

			if (file.isEmpty()) {
				file = dirName + "/Screenshot_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss") + ".png";
			}

			m_pixmap.save(file, "PNG");
			close();

			// Function from utilities.cpp
			CPrime::MessageEngine::showMessage(
						"cc.cubocore.CoreShot",
						"CoreShot",
						"Screenshot Saved",
						"Check the " + CPrime::FileUtils::dirName(file)
			);
		} else {
			CPrime::MessageEngine::showMessage(
						"cc.cubocore.CoreShot",
						"CoreShot",
						"Screenshot Not Saved",
						"Please select a valid directory from CoreGarage."
			);

			// opens preview
			coreshot *shootP = new coreshot();
			shootP->setPixmap(m_pixmap);
			shootP->show();
			this->close();
		}
    } else if (afterShotTaken == 0) {  // opens editor
		QString dirName = "/tmp";
		QFile file(dirName + "/Screenshot_tmp_" + QDateTime::currentDateTime().toString("yyyy-MM-dd_hh-mm-ss") + ".png");
		m_pixmap.save(&file, "PNG");
		file.close();

		QString fileName = file.fileName();
        QString app = smi->getValue("CoreApps", "ImageEditor");

		if (app != "Nothing" || app.length()) {

            CPrime::AppOpenFunc::defaultAppEngine(CPrime::DefaultAppCategory::ImageEditor, QFileInfo(fileName), "");
			this->close();
		} else {
			CPrime::MessageEngine::showMessage(
						"cc.cubocore.CoreShot",
						"CoreShot",
						"Screenshot Not Opened",
						"No default Image Editor. Set one using CoreGarage."
			);

			// opens preview
			coreshot *shootP = new coreshot();
			shootP->setPixmap(m_pixmap);
			shootP->show();
			this->close();
		}
    } else if (afterShotTaken == 1)  {  // opens preview
		coreshot *shootP = new coreshot();
		shootP->setPixmap(m_pixmap);
		shootP->show();
		this->close();
	}
}

void coreshotdialog::shootFullScreen()
{
    // Get the display connection
    Display* display = XOpenDisplay(nullptr);
    if (!display) {
        qCritical() << "Unable to open X11 display";
        return;
    }

    // Get the root window (full screen) of the default screen
    Window root = DefaultRootWindow(display);

    // Get the screen width and height
    int screen = DefaultScreen(display);
    int width = DisplayWidth(display, screen);
    int height = DisplayHeight(display, screen);

    // Capture the screen content into an XImage
    XImage* xImage = XGetImage(display, root, 0, 0, width, height, AllPlanes, ZPixmap);
    if (!xImage) {
        qCritical() << "Failed to capture the screen content";
        XCloseDisplay(display);
        return;
    }

    // Convert the XImage to QImage
    QImage image((uchar*)xImage->data, width, height, QImage::Format_RGB32);
    m_pixmap = QPixmap::fromImage( image.copy() );  // Make a deep copy, as xImage->data is freed later

    // Clean up
    XDestroyImage(xImage);  // Frees the data allocated by XGetImage
    XCloseDisplay(display);

	passTo();
}

void coreshotdialog::shootSelectArea()
{
	if (mousegrabbed) {
		return;
	}

	lastgeom = this->geometry();

	snapArea = QRect();
	this->grabMouse(QCursor(Qt::CrossCursor));
	mousegrabbed = true;
	this->ui->frame->setEnabled(false);
	this->setWindowOpacity(0);
}

void coreshotdialog::shootActiveWindow()
{
	// Current window rectangle.
	WId currWid = getActiveWindowId();

	if (currWid == 0) {
		m_pixmap = QApplication::primaryScreen()->grabWindow(0);
		passTo();
		return;
	}

	QRect rect = getWindowFrame(currWid);

	// Current whole screen.

	QScreen *screen = QGuiApplication::primaryScreen();

	auto cwposX = rect.topLeft().x();
	auto cwposY = rect.topLeft().y();
	auto cwHeight = rect.topLeft().y() + rect.height();
	auto cwWidth = rect.topLeft().x() + rect.width();
	auto scHeight = screen->size().height();
	auto scWidth = screen->size().width();
	auto scposX = screen->availableGeometry().x();
	auto scposY = screen->availableGeometry().y();

	if (cwHeight > scHeight) {
		cwHeight = rect.height() - (cwHeight - screen->availableSize().height());

		if (cwposY < 0) {
			cwposY = scposY;
			cwHeight = cwHeight + rect.y();
		} else {
			cwHeight = cwHeight + scposY;
		}
	} else {
		cwHeight = rect.height();

		if (cwposY < 0) {
			cwposY = scposY;
			cwHeight = rect.y() + rect.height() - cwposY;
		}
	}

	if (cwWidth > scWidth) {
		cwWidth = rect.width() - (cwWidth - screen->availableSize().width());

		if (cwposX < 0) {
			cwposX = scposX;
			cwWidth = cwWidth + rect.x();
		} else {
			cwWidth = cwWidth + scposX;
		}
	} else {
		cwWidth = rect.width();

		if (cwposX < 0) {
			cwposX = scposX;
			cwWidth = rect.x() + rect.width() - cwposX;
		}
	}

	auto pixmap = screen->grabWindow(/*QApplication::desktop()->winId()*/ 0,
				  cwposX, cwposY, cwWidth, cwHeight);

	m_pixmap = pixmap;

	this->hide();
	passTo();
}

quint32 AppRootWindow() {

    if (!qApp) return 0;

    QPlatformNativeInterface *native = qApp->platformNativeInterface();
    if (!native) return 0;

    QScreen *screen = QGuiApplication::primaryScreen();
    if (!screen) return 0;

    return static_cast<xcb_window_t>(reinterpret_cast<quintptr>(native->nativeResourceForScreen("rootwindow", screen)));
}

// fix approotwindow function
WId coreshotdialog::getActiveWindowId() {
	Window root = RootWindow(mDisplay, DefaultScreen(mDisplay));
	Atom atom = XInternAtom(mDisplay, "_NET_ACTIVE_WINDOW", false);
	unsigned long type, resultLen, rest;
	int format;
	WId result = 0;
	unsigned char *data = nullptr;

	// FIX ME
	// Don't know the new cast for this 'XA_CARDINAL' because it's a Macro
	if (XGetWindowProperty(mDisplay, root, atom, 0, 1, false,
						   XA_WINDOW, &type, &format, &resultLen, &rest, &data) == Success) { //cast
		result = static_cast<WId>(*reinterpret_cast<long *>(data));
		XFree(data);
	}

	return result;
}

QRect coreshotdialog::getWindowFrame(WId wid)
{
	QRect result;
	XWindowAttributes wa;

	if (XGetWindowAttributes(mDisplay, wid, &wa)) {
		Window child;
		int x, y;
		// translate to root coordinate
		XTranslateCoordinates(mDisplay, wid, wa.root, 0, 0, &x, &y, &child);
		result.setRect(x, y, wa.width, wa.height);

		// get the frame widths added by the window manager
		Atom atom = XInternAtom(mDisplay, "_NET_FRAME_EXTENTS", false);
		unsigned long type, resultLen, rest;
		int format;
		unsigned char *data = nullptr;

		// FIX ME
		// Don't know the new cast for this 'XA_CARDINAL' because it's a Macro
		if (XGetWindowProperty(mDisplay, wid, atom, 0, LONG_MAX, false,
							   (XA_CARDINAL), &type, &format, &resultLen, &rest, &data) == Success) { //cast
		}

		if (data) { // left, right, top, bottom
			long *offsets = reinterpret_cast<long *>(data);
			result.setLeft(static_cast<int>(result.left() - offsets[0]));
			result.setRight(static_cast<int>(result.right() + offsets[1]));
			result.setTop(static_cast<int>(result.top() - offsets[2]));
			result.setBottom(static_cast<int>(result.bottom() + offsets[3]));
			XFree(data);
		}
	}

	return result;
}

void coreshotdialog::mousePressEvent(QMouseEvent *ev)
{
	if (mousegrabbed) {
		pt_click = ev->globalPosition().toPoint();

		if (areaOverlay == nullptr) {
			areaOverlay =  new QWidget(nullptr, Qt::Window | Qt::BypassWindowManagerHint | Qt::WindowStaysOnTopHint);
			areaOverlay->setWindowOpacity(0.5);
			areaOverlay->setStyleSheet("background-color: rgba(150,150,150,120)");
		}
	}

	QWidget::mouseMoveEvent(ev);
}

void coreshotdialog::mouseMoveEvent(QMouseEvent *ev)
{
	if (mousegrabbed) {
		//Not used yet - do something to paint the area so the user can see which area is selected
		QRect area = pointsToRect(pt_click, ev->globalPosition().toPoint());
		areaOverlay->setGeometry(area);
		areaOverlay->show();
	} else {
		QWidget::mouseMoveEvent(ev);
	}
}

void coreshotdialog::mouseReleaseEvent(QMouseEvent *ev)
{
	if (mousegrabbed) {
		mousegrabbed = false;
		this->ui->frame->setEnabled(true);
		this->releaseMouse();
		this->setWindowOpacity(1);
		areaOverlay->hide();
		snapArea = pointsToRect(pt_click, ev->globalPosition().toPoint());
        this->hide();
        QTimer::singleShot((ui->delayCapture->text().toInt() * 1000) + 200, this, SLOT(getPix())); // Delay
	} else {
		QWidget::mouseReleaseEvent(ev); //normal processing
	}
}

QRect coreshotdialog::pointsToRect(QPoint pt1, QPoint pt2)
{
	QRect rec;

	if (pt1.x() < pt2.x()) {
		rec.setLeft(pt1.x());
		rec.setRight(pt2.x());
	} else {
		rec.setLeft(pt2.x());
		rec.setRight(pt1.x());
	}

	if (pt1.y() < pt2.y()) {
		rec.setTop(pt1.y());
		rec.setBottom(pt2.y());
	} else {
		rec.setTop(pt2.y());
		rec.setBottom(pt1.y());
	}

	return rec;
}

void coreshotdialog::getPix()
{
	QScreen *scrn = QApplication::primaryScreen()/*screens().at(0)*/;
	m_pixmap = scrn->grabWindow(/*QApplication::desktop()->winId()*/0, snapArea.x(), snapArea.y(), snapArea.width(), snapArea.height());

	this->showNormal();
	this->setGeometry(lastgeom);
	passTo();
}

void coreshotdialog::on_delayCapture_clicked()
{
    // increase the mDelay number
    mDelay = ( mDelay + 1 ) % 11;

    ui->delayCapture->setText("Delay(s): " + QString::number(mDelay));
}


void coreshotdialog::on_afterShotTaken_clicked()
{
    // increase the afterShotTaken number
    afterShotTaken = ( afterShotTaken + 1 ) % 2;

    // set text in the afterShotTaken button
    if ( afterShotTaken == 0 ){
        ui->afterShotTaken->setText("Open Image Editor");
    } else if ( afterShotTaken == 1 ){
        ui->afterShotTaken->setText("Open Preview");
    } else if ( afterShotTaken == 2 ){
        ui->afterShotTaken->setText("Auto Save");
    }
}

void coreshotdialog::closeEvent(QCloseEvent *event)
{
    event->ignore();

    qDebug()<< "save window stats"<< afterShotTaken << mDelay;
    smi->setValue("CoreShot", "AfterShotTaken", afterShotTaken);
    smi->setValue("CoreShot", "Delay", mDelay);

    event->accept();
}
